/// <reference types="vitest" />
/// <reference types="vite-ssg" />

import { resolve } from 'node:path'
import { unheadVueComposablesImports } from '@unhead/vue'
import vue from '@vitejs/plugin-vue'
import unocss from 'unocss/vite'
import autoImport from 'unplugin-auto-import/vite'
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers'
import vueComponents from 'unplugin-vue-components/vite'
import { defineConfig } from 'vite'
import pages from 'vite-plugin-pages'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => ({
  plugins: [
    // https://github.com/unocss/unocss
    unocss(),
    // https://github.com/antfu/unplugin-auto-import
    autoImport({
      imports: ['vue', 'vue-router', '@vueuse/core', unheadVueComposablesImports],
      dts: 'src/auto-imports.d.ts',
    }),
    // https://github.com/vitejs/vite/tree/main/packages/plugin-vue
    vue(),
    // https://github.com/antfu/unplugin-vue-components
    vueComponents({
      dts: 'src/vue-components.d.ts',
      resolvers: [
        NaiveUiResolver(),
      ],
    }),
    // https://github.com/hannoeru/vite-plugin-pages
    pages({
      dirs: [
        { dir: 'src/pages', baseRoute: '' },
        { dir: 'src/examples', baseRoute: 'examples' },
      ].slice(0, (command === 'build' && mode !== 'test') ? -1 : undefined),
    }),
  ],
  resolve: {
    alias: {
      '~/': `${resolve(__dirname, './src')}/`,
      '@/': `${resolve(__dirname, './src')}/`,
    },
  },
  ssgOptions: {
    formatting: 'prettify',
    dirStyle: 'nested',
  },
  ssr: {
    noExternal: [
      'naive-ui',
      'vueuc',
      '@css-render/vue3-ssr',
      '@css-render/plugin-bem',
      '@juggle/resize-observer',
      'date-fns',
      'gsap',
    ],
  },
  build: {
    rollupOptions: {
      output: {
        manualChunks: {
          'naive-ui': ['naive-ui'],
        },
      },
    },
  },
}))
