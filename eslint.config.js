import byyuurin from '@byyuurin/eslint-config'

export default byyuurin({
  typescript: {
    overrides: {
      'ts/ban-ts-comment': [
        'error',
        {
          'ts-expect-error': 'allow-with-description',
          'ts-ignore': false,
          'ts-nocheck': false,
          'ts-check': false,
          'minimumDescriptionLength': 3,
        },
      ],
    },
  },
  formatters: {
    prettierOptions: {
      singleQuote: false,
    },
  },
  rules: {
    'no-console': 'off',
  },
})
