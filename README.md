# Vue 3 + Vite + Vite SSG (optional)

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Require VS Code Extensions

- [Vue Language Features (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable `Vetur`)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [UnoCSS](https://marketplace.visualstudio.com/items?itemName=antfu.unocss)

---

<table><tr><td width="400px" valign="top">

### 建立專案

</td><td width="600px"><br>

透過此專案範本建立新專案

```bash
npx tiged https://gitlab.com/yuurin.kyart/starter-vue-html [YOUR_PROJECT_NAME]
```

</td></tr></table>

<table><tr><td width="400px" valign="top">

### 相關指令

套件安裝後**需要**重開一次 VS Code 讓擴充功能運作

</td><td width="600px"><br>

安裝

```bash
npm install
```

開發

```bash
npm run dev
```

建構頁面

```bash
npm run generate
```

</td></tr></table>

---

<table><tr><td width="400px" valign="top">

### 環境要求

NodeJS >= 18

</td><td width="600px"><br>

查看目前 NodeJS 版本

```bash
node -v
```

查看所有 NodeJS 版本

```bash
nvm list
```

增加其他 NodeJS 版本 [🔍](https://nodejs.org/en/about/previous-releases)

```bash
nvm install 版本號
```

切換 NodeJS 版本

```bash
nvm use 版本號
```

</td></tr></table>
## Q/A

[docs](docs)
