import type { Fn, MaybeRefOrGetter } from '@vueuse/core'
import { toRef } from '@vueuse/core'
import type { FormItemRule, FormValidationError, NForm } from 'naive-ui'

export type FormModelRules<T = string> = Record<keyof T, FormItemRule | FormItemRule[]>

export function useForm<T extends Record<string, any>>(initialData: MaybeRefOrGetter<T>, rules: Partial<FormModelRules<T>> = {}) {
  const validateErrors = ref<FormValidationError[]>([])
  const formData = toRef(initialData)
  const formRef = ref<InstanceType<typeof NForm>>()
  const { cloned: formModel, sync } = useCloned(formData)
  const formError = computed<Partial<Record<keyof T, string>>>(() => Object.fromEntries(validateErrors.value.map((group) => [group[0].field, group[0].message])))

  function clear() {
    validateErrors.value = []
    formRef.value?.restoreValidation()
  }

  function resetForm() {
    clear()
    sync()
  }

  function submit(resolve: Fn, reject?: Fn) {
    clear()

    setTimeout(() => {
      formRef.value?.validate((errors) => {
        if (errors) {
          validateErrors.value = errors
          reject?.()
          return
        }

        validateErrors.value = []
        resolve()
      }).catch(() => {})
    }, 0)
  }

  return {
    formRef,
    formModel,
    formRules: rules as FormModelRules,
    formError,
    resetForm,
    submit,
  }
}
