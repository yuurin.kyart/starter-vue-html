import { createPinia } from 'pinia'
import type { AppPlugin } from '~/types'

export const install: AppPlugin = ({ app, isClient, initialState }) => {
  const pinia = createPinia()

  app.use(pinia)

  if (isClient)
    pinia.state.value = initialState.pinia || {}
  else
    initialState.pinia = pinia.state.value
}
