import { setup } from '@css-render/vue3-ssr'
import type { AppPlugin } from '~/types'

export const install: AppPlugin = ({ app, isClient }) => {
  if (!isClient)
    setup(app)
}
