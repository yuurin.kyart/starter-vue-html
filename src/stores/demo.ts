import { defineStore } from 'pinia'

export const useDemoStore = defineStore('demo', () => {
  const foo = ref('Hello world')

  return {
    foo,
  }
})
