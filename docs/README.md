## 開發常見問題整理

<table><tr><td width="400px" valign="top">

### 如何修改頁面標題？

- 使用 [`useSeoMeta`](https://unhead.harlanzw.com/guide/composables/use-seo-meta#useseometa) 進行設定
- 在 `src/App.vue` 設定全站預設標題
- 在 `src/pages/*.vue` 設定該頁面標題

</td><td width="600px"><br>

```html
<!-- src/App.vue -->
<script setup>
  useSeoMeta({
    title: "標題",
    titleTemplate: "%s - 網站名稱",
  })
</script>
```

```html
<title>標題 - 網站名稱</title>
```

---

```html
<!-- src/pages/*.vue -->
<script setup>
  useSeoMeta({
    title: "頁面標題",
  })
</script>
```

```html
<title>頁面標題 - 網站名稱</title>
```

---

```html
<!-- src/pages/*.vue -->
<script setup>
  useSeoMeta({
    title: '頁面標題'
    titleTemplate: '%s | 網站名稱'
  })
</script>
```

```html
<title>頁面標題 | 網站名稱</title>
```

</td></tr></table>

---

<table><tr><td width="400px" valign="top">

### 如何修改 SEO META？

- 使用 [`useSeoMeta`](https://unhead.harlanzw.com/guide/composables/use-seo-meta#useseometa) 進行設定
- 若有需要定其它 meta 屬性時可使用 [`useHead`](https://unhead.harlanzw.com/guide/composables/use-head) 進行設定, 例: `keywords`

</td><td width="600px"><br>

設定內容

```html
<script setup>
  useSeoMeta({
    description: "My about page",
    ogDescription: "Still about my about page",
    ogTitle: "About",
    ogImage: "https://example.com/image.png",
    twitterCard: "summary_large_image",
  })
  useHead({
    meta: [{ name: "keywords", content: "keyword1, keyword2, keyword3" }],
  })
</script>
```

輸出結果

```html
<meta name="description" content="My about page" />
<meta name="keywords" content="keyword1, keyword2, keyword3" />
<meta property="og:description" content="Still about my about page" />
<meta property="og:title" content="About" />
<meta property="og:image" content="https://example.com/image.png" />
<meta name="twitter:card" content="summary_large_image" />
```

</td></tr></table>

---

<table><tr><td width="400px" valign="top">

### 如何加入圖示？

前往 [Icônes](https://icones.js.org/collection/all) 找到需要的圖示並複製名稱

複製名稱後作為 class 使用

</td><td width="600px"><br>

![icones](images/icones.png)

```html
<i class="i-vscode-icons-file-type-unocss"></i>
```

</td></tr></table>
